#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

//MAXIMO DE CLIENTES CADASTRADOS
#define MAX 50

typedef struct cliente cliente;
typedef struct conta_corrente conta_corrente;
typedef struct transacao transacao;
typedef struct lista_cliente lista_cliente;
typedef struct lista_conta_corr lista_conta_corr;
typedef struct no_transacao no_transacao;
typedef struct l_transacao l_transacao;


//ESTRUTURAS TRANSACAO, CONTA CORRENTE E CLIENTES
struct transacao
{
	int cod_transacao;
	char tipo_transacao;
	int origem;
	int destino;
	float valor;
};

struct conta_corrente
{
	int cod_conta;
	float saldo;
	transacao lista_transacao;
};

struct cliente
{
	int cod_cli;
	char nome_cli[50];
	char telefone[20];
	int conta_cli;
};

//TAD LISTA SEQUENCIAL CLIENTE, CONTA CORRENTE
struct lista_cliente{
    int quant_cli;// NUMERO DE ELEMENTOS ATUAL DA LISTA
    cliente l_cli[MAX];
};

struct lista_conta_corr{
    int quant_conta_corr;// NUMERO DE ELEMENTOS ATUAL DA LISTA
    conta_corrente l_conta_corr[MAX];
};

//TAD LISTA ENCADEADA TRANSACAO
struct no_transacao
{
	transacao info;
	no_transacao *proximo;
};
struct l_transacao
{
	int nelem;
	no_transacao *head;
};

//INICIALIZA LISTAS
void inicializa_lista(l_transacao *lista_transacao,lista_conta_corr *lista_conta_corr,lista_cliente *lista_cliente)
{
    lista_cliente->quant_cli=0; //INICIALIZA CLIENTE
    lista_conta_corr->quant_conta_corr=0;//INICIALIZA CONTA CORRENTE

    lista_transacao = (l_transacao*) malloc(sizeof(l_transacao));//ALOCA ESPA�O PARA A TRANSACAO
    lista_transacao->head = NULL;// INICIALIZA A LISTA
    lista_transacao->nelem = 0; // NUMERO DE ELEMENTOS COME�A EM ZERO
}

//INSERE CONTAS CORRENTES
void inserir_conta_correntes(lista_conta_corr *lista_conta_corr)
{
	int i;
	for(i=0;i<MAX;i++)
	{
		lista_conta_corr->l_conta_corr[i].cod_conta=i+1;
		lista_conta_corr->l_conta_corr[i].saldo=-1;
		lista_conta_corr->quant_conta_corr++;
	}
}

//LISTA TODAS AS TRANSACOES
void imprimir_transacao(l_transacao *lista)
{
    no_transacao *posicao;
    posicao = lista->head;
    int i;
	while(posicao != NULL)
	{
		printf("Codigo: %i\n", posicao->info.cod_transacao);
		printf("Destino: %i\n", posicao->info.destino);
		printf("Origem: %i\n", posicao->info.origem);
		printf("Tipo: %c\n", posicao->info.tipo_transacao);
		printf("Valor: %f\n", posicao->info.valor);
		printf("\n\n");
		posicao = posicao->proximo;
		i++;
	}

}
//BUSCA CLIENTE PARA INSERIR NOVO CLIENTE NA POSICAO CORRETA
int busca_cliente(lista_cliente lista,cliente novo_cliente)
{
	int i=0;
	//PERCORRE LISTA PARA ENCONTRAR POSICAO DE INSERCAO DO CLIENTE
	while(lista.l_cli[i].cod_cli<novo_cliente.cod_cli)
	{
		i++;
	}
	return i;
}

//VERIFICA CONTAS CORRENTES DISPONIVEIS
int busca_conta_disponivel(int conta,lista_cliente lista_cliente)
{
	int j;
		for(j=0;j<lista_cliente.quant_cli;j++)
		{
			if(conta==lista_cliente.l_cli[j].conta_cli)
			{
				return 1;
			}
		}

		return 0;
}

//VERIFICA CONTAS CORRENTES EXISTENTES
int busca_contas_existentes(int conta,lista_conta_corr lista_conta_corr)
{
	int j;
		for(j=0;j<lista_conta_corr.quant_conta_corr;j++)
		{
			if(conta==lista_conta_corr.l_conta_corr[j].cod_conta)
			{
				return 0;
			}
		}

		return 1;
}

//BUSCA POSICAO DA CONTA CORRENTE DESEJADA PARA A INSERCAO DE SALDO INICIAL
int busca_posicao_conta(int conta,lista_conta_corr lista_conta_corr)
{
	int j;
		for(j=0;j<lista_conta_corr.quant_conta_corr;j++)
		{
			if(conta==lista_conta_corr.l_conta_corr[j].cod_conta)
			{
				return j;//RETORNA POSICAO DA CONTA CORRENTE BUSCADA
			}
		}

		return 0;
}

//INSERIR TRANSACOES
void inserir_transacao(l_transacao *l_transacao,transacao nova_transacao)
{
	no_transacao *elemento;
	no_transacao *aux;

	if(l_transacao->nelem==0)
	{
		elemento=malloc(sizeof(no_transacao));
		elemento->info=nova_transacao;
		elemento->proximo=NULL;
		l_transacao->head=elemento																				;
		l_transacao->nelem=1;
	}
	else
	{
		elemento=(no_transacao*)malloc(sizeof(no_transacao));
		elemento->info=nova_transacao;
		aux=l_transacao->head;
		l_transacao->head=elemento;
		elemento->proximo=aux;
		l_transacao->nelem++;
	}
}

//INSERE CLIENTES
void inserir_cliente(cliente novo_cliente,conta_corrente nova_conta,lista_cliente *lista_cliente, lista_conta_corr *lista_conta_corr)
{
	int pos,i,pos_conta;

	//VERIFICA SE A LISTA DE CLIENTES ESTA CHEIA
	if(lista_cliente->quant_cli==MAX)
	{
		printf("Lista de cliente est� cheia");
	}
	else
	{
		//VERIFICA SE A LISTA ESTA VAZIA
		if(lista_cliente->quant_cli==0)
		{
			//INSERE DADOS NA POSICAO 0
			lista_cliente->l_cli[0].cod_cli=lista_cliente->quant_cli+1;
			strcpy(lista_cliente->l_cli[0].nome_cli,novo_cliente.nome_cli);
			strcpy (lista_cliente -> l_cli [0].telefone,novo_cliente.telefone);



			//RECEBE NUMERO DE CONTA CORRENTE
			lista_cliente->l_cli[0].conta_cli=novo_cliente.conta_cli;

			//INSERE SALDO INICIAL
			pos_conta=busca_posicao_conta(novo_cliente.conta_cli,*lista_conta_corr);
			lista_conta_corr->l_conta_corr[pos_conta].saldo=nova_conta.saldo;


			//AUMENTA QUANTIDADE DE CLIENTES
			lista_cliente->quant_cli++;


		}
		else
		{
			//BUSCA POSICAO DE INSERCAO
			pos=busca_cliente(*lista_cliente,novo_cliente);

			//MOVIMENTA CLIENTES PARA FRENTE PARA ARMAZENAR NOVO CLIENTE NA LISTA
			for(i=lista_cliente->quant_cli;i>pos;i--)
			{
				lista_cliente->l_cli[i]=lista_cliente->l_cli[i-1];
			}

			//INSERIR CLIENTE NO LOCAL APROPRIADO
			lista_cliente->l_cli[i].cod_cli=lista_cliente->quant_cli+1;
			strcpy(lista_cliente->l_cli[pos].nome_cli,novo_cliente.nome_cli);
			strcpy (lista_cliente -> l_cli [pos].telefone,novo_cliente.telefone);

			//RECEBE NUMERO DE CONTA CORRENTE
			lista_cliente->l_cli[pos].conta_cli=novo_cliente.conta_cli;

			//INSERE SALDO INICIAL
			pos_conta=busca_posicao_conta(novo_cliente.conta_cli,*lista_conta_corr);
			lista_conta_corr->l_conta_corr[pos_conta].saldo=nova_conta.saldo;


			//AUMENTA QUANTIDADE DE CLIENTES
			lista_cliente->quant_cli++;

		}
	}
}

//BUSCA TRANSACOES DA CONTA CORRENTE DESEJADA
void consulta_transacao(int cod_conta, l_transacao *l_transacao)
{
	no_transacao *posicao;
    posicao = l_transacao->head;

    printf("\nLista de Transa��es\n");
    while(posicao != NULL)
    {
    	printf("%i\n",cod_conta);
        if((posicao->info.destino == cod_conta) || (posicao->info.origem == cod_conta))
        {
			printf("Destino: %i \nOrigem: %i \nTipo:%c \nValor:%0.2f\n\n",posicao->info.destino,posicao->info.origem,posicao->info.tipo_transacao,posicao->info.valor);
			posicao = posicao->proximo;
		}
        else
        {
        	posicao = posicao->proximo;
		}


    }

}

//BUSCA SALDO DA CONTA CORRENTE DESEJADA
float verifica_saldo(int cod_conta, lista_conta_corr lista_conta_corr)
{
	int j;
   for(j=0;j<lista_conta_corr.quant_conta_corr;j++)
	{
        if(cod_conta==lista_conta_corr.l_conta_corr[j].cod_conta)
		{
        	return lista_conta_corr.l_conta_corr[j].saldo;
		}

    }
    return 0;

}


//CONSULTA SE UM CLIENTE EXISTE POR MEIO DE BUSCA SEQUENCIAL PELO CODIGO DE CONTA CORRENTE
int consultar_cliente (lista_cliente lista_cliente,int n,int cod_conta)
{
 int i;
 for (i=0;i<n;i++)
 {
     if (lista_cliente.l_cli[i].conta_cli==cod_conta)
     {
       printf("Nome: %s		|| 		Telefone: %s\n",lista_cliente.l_cli[i].nome_cli, lista_cliente.l_cli[i].telefone);
       return i;
     }
  }
	return -1;
}

//ALTERAR CLIENTE
void alterar_cliente (lista_cliente *lista_cliente, int pos_alter)
{
   char novo_nome [50];
   char novo_telefone [20];

   fflush (stdin);
   printf("Digite o novo nome: ");
   scanf("%[^\n]s",&novo_nome);
   fflush (stdin);

   printf("\n\nDigite o novo telefone: ");
   scanf("%[^\n]s",&novo_telefone);
   fflush (stdin);


   //RECEBE O NOVO NOME E O NOVO TELEFONE
   strcpy(lista_cliente->l_cli[pos_alter].nome_cli,novo_nome);
   strcpy(lista_cliente->l_cli[pos_alter].telefone,novo_telefone);

   //IMPRIME AS ALTERA��ES
   printf("Nome: %s || Telefone: %s",lista_cliente->l_cli[pos_alter].nome_cli, lista_cliente->l_cli[pos_alter].telefone);
   printf("\nAlterado com sucesso!");
}

// REMOVE CLIENTE
void deletar_cliente (lista_cliente *lista_cliente, int pos_del)
{
   //MOVIMENTA OS ELEMENTOS DA LISTA SOBRE OS ELEMENTOS QUE EST�O SENDO SENDO DELETADOS.

    int i;

    for(i=pos_del;i<lista_cliente->quant_cli-1;i++)
    lista_cliente->l_cli[i] = lista_cliente->l_cli[i+1];
    lista_cliente->quant_cli--;
}

//CONSULTA POSI��O DA CONTA CORRENTE BUSCADA
int consultar_conta_corr (lista_conta_corr lista_conta_corr,int n,int cod_conta)
{
 int i;
 for (i=0;i<n;i++)
 {
     if (lista_conta_corr.l_conta_corr[i].cod_conta==cod_conta)
     {
       	return i;
     }
     return -1;
  }
}

//DELETAR TRANSACAO

//BUSCA PARA DELETAR

void busca_del_transacao(l_transacao *lista, int cod_conta)
{
    int i;
    no_transacao * posicao;
    no_transacao *aux;
    no_transacao* p= NULL;
    p = lista->head;

	while(p->proximo!=NULL)
	{
           if((p->info.destino == cod_conta || p->info.origem == cod_conta))
               {
                posicao = NULL; // ACHOU TRANSACAO NA PRIMEIRA POSICAO
               }

            if((p->proximo->info.destino == cod_conta || p->proximo->info.origem == cod_conta))
            {
                p->proximo=NULL;

            }
            p=p->proximo;
    }
}

void deletar_transacao (l_transacao *lista, int cod_conta){

  if(lista->nelem == 1)
    {
        if(lista->head->info.destino== cod_conta)
        {
            lista->head = NULL;
            lista->nelem = 0;
            printf("Cliente deletado com sucesso!");
        }
    }
    else
    {
        busca_del_transacao (lista,cod_conta);
        printf("Cliente deletado com sucesso!");
    }
}


void interface(lista_cliente lista_cliente,lista_conta_corr lista_conta_corr,l_transacao *l_transacao)
{
	//DECLARA��O DE VARIAVEIS
	int op, encerra=0;
	cliente novo_cliente;
	conta_corrente nova_conta;
	transacao nova_transacao;
	int codigo,cod_conta,cod_conta_alter,cod_conta_del,transferencia,alter_conta_corr,alter_conta_transf,cont_tab,consultar=3,verifica_deposito,verifica_saque,verifica_transferencia;
	int i, verifica_conta,verifica_conta_disponivel,verifica_conta_existente,nelem,cont,pos_alter,verifica_cliente, pos_del, verifica_del_cliente,verifica_alter_cliente;
	float saldo;

	//MENU
	while(encerra==0)
	{
		inicio:
		printf("********** SISTEMA BANC�RIO **********\n");
		printf("*------------ 1- Cliente ------------*\n");
		printf("*----------2- Transa��es ------------*\n");
		printf("*------------- 3- Sair --------------*\n");
		printf("OP��O: \n");
		scanf("%i",&op);
		system("cls");
		switch(op)
		{
			case 1:
			printf("***************** CLIENTE *****************\n");
			printf("*-------- 1- Inserir novo Cliente --------*\n");
			printf("*---- 2- Consultar dados de um Cliente----*\n");
			printf("*-- 3- Atualizar cadastro de um Cliente --*\n");
			printf("*---------- 4- Remover Cliente -----------*\n");
			printf("*------- 5- Ver lista de Clientes --------*\n");
			printf("*--------------- 6- Voltar ---------------*\n");
			printf("OP��O: \n");
			scanf("%i",&op);
			system("cls");

			switch(op)
			{
				case 1:
					//INSERIR NOVO CLIENTE
					printf ("Digite o numero de clientes que deseja inserir: ");
					scanf ("%i",&nelem);

					system("cls");
					for (cont=0;cont<nelem;cont++)
					{
						fflush(stdin);
						printf("Entre com o nome do cliente: ");
				    	fflush(stdin);
						scanf("%[^\n]s",&novo_cliente.nome_cli);
						fflush(stdin);
						printf ("Entre com o telefone do cliente: ");
				    	fflush(stdin);
						scanf("%[^\n]s",&novo_cliente.telefone);
						fflush(stdin);

						//lISTA CONTAS DISPONIVEIS
						printf("\n\nContas Correntes Disponiveis\n\n");
						cont_tab=0;
				    	for(i=0;i<lista_conta_corr.quant_conta_corr;i++)
						{

					    	verifica_conta=busca_conta_disponivel(lista_conta_corr.l_conta_corr[i].cod_conta,lista_cliente);
	    					if(verifica_conta==0)
    						{
	    						printf("	%i		",lista_conta_corr.l_conta_corr[i].cod_conta);
	    						cont_tab++;
							}
							if(cont==5)
							{
								cont_tab=0;
								printf("\n");
							}

	    				}

						printf("\nDigite o numero da conta corrente escolhida: ");
					    fflush(stdin);
						scanf("%d",&novo_cliente.conta_cli);
					    fflush(stdin);

						//VERIFICA SE A CONTA CORRENTE DIGITA � VALIDA
						verifica_conta_disponivel=busca_conta_disponivel(novo_cliente.conta_cli,lista_cliente);
				    	verifica_conta_existente=busca_contas_existentes(novo_cliente.conta_cli,lista_conta_corr);

						while(verifica_conta_disponivel==1 || verifica_conta_existente==1)
					    {
					    	printf("\nConta Indisponivel");
					    	printf("\nDigite o numero da conta corrente escolhida: ");
							fflush(stdin);
							scanf("%i",&novo_cliente.conta_cli);
    						fflush(stdin);
							verifica_conta_disponivel=busca_conta_disponivel(novo_cliente.conta_cli,lista_cliente);
							verifica_conta_existente=busca_contas_existentes(novo_cliente.conta_cli,lista_conta_corr);
						}

						printf("Digite o deposito inicial: ");
						fflush(stdin);
						scanf("%f",&nova_conta.saldo);
						fflush(stdin);

						//VERIFICA SE O DEPOSITO INICIAL ESTA ACIMA DO VALOR PERMITIDO
						while(nova_conta.saldo<50)
						{
							printf("\nSaldo Invalido! Deposite um valor acima de 50 reais\n");
							fflush(stdin);
							scanf("%f",&nova_conta.saldo);
							fflush(stdin);
						}
						inserir_cliente(novo_cliente,nova_conta,&lista_cliente,&lista_conta_corr);

						nova_transacao.cod_transacao=l_transacao->nelem+1;
	    				nova_transacao.origem=NULL;
					    nova_transacao.destino=novo_cliente.conta_cli;
					    nova_transacao.tipo_transacao='D';
					    nova_transacao.valor=nova_conta.saldo;
						inserir_transacao(&l_transacao,nova_transacao);
						printf("Cadastrado com sucesso!");
						getch();
						system("cls");
					}

				break;

					case 2:
						//CONSULTA DE UM CLIENTE
						printf("Digite o numero da conta corrente: ");
						fflush(stdin);
						scanf("%i", &cod_conta);
						fflush(stdin);
						//BUSCA CLIENTE
						verifica_cliente = consultar_cliente(lista_cliente,lista_cliente.quant_cli,cod_conta);
						//CASO CLIENTE N�O SEJA ENCONTRADO
						while(verifica_cliente==-1)
						{
							printf("Cliente n�o encontrado!");
							getch();
							system("cls");
							consultar=3;
							while(consultar==3)
							{
								printf("Deseja consultar outra conta corrente: [1]-SIM [2]-VOLTAR");
								fflush(stdin);
								scanf("%i",&consultar);
								fflush(stdin);
								if(consultar==1)
								{
									printf("Digite o numero da conta corrente: ");
									fflush(stdin);
									scanf("%i", &cod_conta);
									fflush(stdin);
									verifica_cliente = consultar_cliente (lista_cliente,lista_cliente.quant_cli,cod_conta);

								}
								else if(consultar==2)
								{
									consultar=0;
									verifica_cliente=0;
									system("cls");
									goto inicio;

								}
								else
								{
									consultar=3;
									printf("Op��o inv�lida!");
									getch();
									system("cls");
								}
							}
						}
						fflush(stdin);
						consulta_transacao(cod_conta,&l_transacao);
						getch();
						system("cls");
					break;

						case 3:
							//ALTERAR CLIENTE
							printf("Digite o numero da conta corrente do cliente o qual deseja alterar os dados cadastrais: ");
							scanf("%i", &cod_conta_alter);
							//BUSCA POSICAO DO CLIENTE A SER ALTERADO
							pos_alter = consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_alter);

							if(pos_alter!=-1)
    						{
								verifica_alter_cliente=4;
							}
							while (pos_alter == -1)
    						{
    							repetir:
    		    				printf("Cliente n�o encontrado!");
    		    				getch();
								system("cls");
								printf("Digite o numero da conta corrente do cliente o qual deseja alterar os dados cadastrais: ");
								scanf("%i", &cod_conta_alter);
								//BUSCA POSICAO DO CLIENTE A SER ALTERADO
								pos_alter = consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_alter);
    							if(pos_alter!=-1)
    							{
									verifica_alter_cliente=4;
								}
    							while(verifica_alter_cliente==4)
    							{
									printf("\n\nDeseja realmente alterar esse cliente [1]SIM [2]NAO [3]SAIR \n\n");
    								scanf("%i",&verifica_alter_cliente);
									system("cls");
						    		if (verifica_alter_cliente==1)
						   			{
       									alterar_cliente(&lista_cliente,pos_alter);
       									getch();
										system("cls");
    								}

    								while (verifica_alter_cliente==2)
    								{
        								//ALTERAR CLIENTE
		        						printf("\n\n");
    						    		printf("Digite o numero da conta corrente do cliente o qual deseja alterar os dados cadastrais: ");
        								scanf("%i", &cod_conta_alter);

	    					    		pos_alter = consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_alter);
	    					    		if(pos_alter==-1)
	    					    		{
	    					    			goto repetir;
										}
	        							verifica_alter_cliente=4;
    								}

     								if (verifica_alter_cliente==3)
    								{
										getch();
										system("cls");
        								goto inicio;
    								}
								}
							}
							while(verifica_alter_cliente==4)
    							{
									printf("\n\nDeseja realmente alterar esse cliente [1]SIM [2]NAO [3]SAIR \n\n");
    								scanf("%i",&verifica_alter_cliente);

						    		if (verifica_alter_cliente==1)
						   			{
        								//ALTERAR CLIENTE
       									alterar_cliente(&lista_cliente,pos_alter);
    								}

    								while (verifica_alter_cliente==2)
    								{
		        						printf("\n\n");
    						    		printf("Digite o numero da conta corrente do cliente o qual deseja alterar os dados cadastrais: ");
        								scanf("%i", &cod_conta_alter);

	    					    		pos_alter = consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_alter);
	    					    		if(pos_alter==-1)
	    					    		{
	    					    			goto repetir;
										}
	        							verifica_alter_cliente=4;
    								}

     								if (verifica_alter_cliente==3)
    								{
										getch();
										system("cls");
        								goto inicio;
    								}
								}
						break;

							case 4:
								//REMOVER CLIENTE
								printf("Digite o n�mero da conta corrente que deseja remover: ");
    							fflush(stdin);
							    scanf("%i",&cod_conta_del);

							    pos_del= consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_del);

								if (pos_del != -1)
    							{
        							verifica_del_cliente=4;
    							}

    							while (pos_del == -1)
    							{
    								repetir_del:
        							printf("Cliente n�o encontrado!");
        							getch();
									system("cls");

									printf("Digite o n�mero da conta corrente que deseja remover: ");
    								fflush(stdin);
							    	scanf("%i",&cod_conta_del);
							    	pos_del= consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_del);

									if(pos_del!=-1)
									{
										verifica_del_cliente=4;
									}

									while(verifica_del_cliente==4)
									{
    									printf("Deseja realmente excluir esse cliente [1]SIM [2]NAO [3]SAIR \n\n");
    									scanf ("%i",&verifica_del_cliente);
										system("cls");
    									if (verifica_del_cliente==1)
    									{
      										deletar_cliente(&lista_cliente, pos_del);
      										deletar_transacao (&l_transacao,cod_conta_del);
      										getch();
											system("cls");
										}

    									while (verifica_del_cliente==2)
    									{
    										printf("Digite o numero da conta corrente que deseja remover: \n\n");
    										fflush(stdin);
    										scanf("%i",&cod_conta_del);

											pos_del= consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_del);

    										if(pos_del==-1)
    										{
    											goto repetir_del;
											}
											verifica_del_cliente=4;
										}

    									if (verifica_del_cliente==3)
										{
											getch();
											system("cls");
											goto inicio;
       									}
       								}
       							}
       							while(verifica_del_cliente==4)
									{
    									printf("Deseja realmente excluir esse cliente [1]SIM [2]NAO [3]SAIR \n\n");
    									scanf ("%i",&verifica_del_cliente);
										system("cls");
    									if (verifica_del_cliente==1)
    									{
      										deletar_cliente(&lista_cliente, pos_del);
      										deletar_transacao (&l_transacao,cod_conta_del);
											getch();
											system("cls");
										}

    									while (verifica_del_cliente==2)
    									{
    										printf("Digite o numero da conta corrente que deseja remover: \n\n");
    										fflush(stdin);
    										scanf("%i",&cod_conta_del);

											pos_del= consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_del);

    										if(pos_del==-1)
    										{
    											goto repetir_del;
											}
											verifica_del_cliente=4;
										}

    									if (verifica_del_cliente==3)
										{
											getch();
											system("cls");
											goto inicio;
       									}
       								}
							break;

								case 5:
									printf("\nLista de Cliente\n\n");

								    for(i=0;i<lista_cliente.quant_cli;i++)
								    {
								    	printf("C�digo: %i    |Nome: %s  |Telefone:%s	|Conta:%i\n\n",lista_cliente.l_cli[i].cod_cli,lista_cliente.l_cli[i].nome_cli,lista_cliente.l_cli[i].telefone,lista_cliente.l_cli[i].conta_cli);
    								}
									getch();
									system("cls");
								break;

									case 6:
										//VOLTAR
										goto inicio;
									break;


			}
			break;

			case 2:
				printf("*************** TRANSA��ES  ******************\n");
				printf("*----------1- Dep�sito------------------------*\n");
				printf("*----------2- Saque---------------------------*\n");
				printf("*----------3- Transfer�ncia-------------------*\n");
				printf("*----------4- Imprimir lista de transa��es----*\n");
				printf("*----------5- Voltar--------------------------*\n");
				printf("OP��O: \n");
				scanf("%i",&op);

				switch(op)
				{
				case 1:
					//deposito
					printf("Digite o c�digo de conta corrente: ");
					scanf("%i",&cod_conta);

					verifica_conta_disponivel=busca_conta_disponivel(cod_conta,lista_cliente);

					while(verifica_conta_disponivel==0)
					{
						printf("Conta indispon�vel");
						getch();
						system("cls");
						verifica_deposito=3;
						while(verifica_deposito==3)
						{
							printf("Deseja depositar em outra conta: [1]SIM [2]N�O\n");
							scanf("%i",&verifica_deposito);
							if(verifica_deposito==1)
							{
								printf("Digite o c�digo de conta corrente: ");
								scanf("%i",&cod_conta);
								verifica_conta_disponivel=busca_conta_disponivel(cod_conta,lista_cliente);
							}
							else if(verifica_deposito==2)
							{
								verifica_deposito=0;
								system("cls");
								goto inicio;
							}
							else
							{
								verifica_deposito=3;
								printf("Op��o Inv�lida!");
								getch();
								system("cls");
							}
						}
					}
					printf("Digite o valor a ser depositado: ");
					scanf("%f",&nova_transacao.valor);
					while(nova_transacao.valor<=0)
					{
						printf("\nValor inv�lido!");
						getch();
						system("cls");
						printf("Digite o valor a ser depositado: ");
						scanf("%f",&nova_transacao.valor);
					}

					nova_transacao.cod_transacao=l_transacao->nelem+1;
					nova_transacao.origem=NULL;
	    			nova_transacao.destino=cod_conta;
					nova_transacao.tipo_transacao='D';
					inserir_transacao(&l_transacao,nova_transacao);
					alter_conta_corr=(lista_conta_corr,lista_conta_corr.quant_conta_corr,cod_conta);
					lista_conta_corr.l_conta_corr[alter_conta_corr].saldo=lista_conta_corr.l_conta_corr[alter_conta_corr].saldo+nova_transacao.valor;

					printf("Depositado com sucesso!");
					getch();
					system("cls");
				break;

					case 2:
						printf("Digite o c�digo de conta corrente que deseja sacar: ");
						scanf("%i",&cod_conta);

						verifica_conta_disponivel=busca_conta_disponivel(cod_conta,lista_cliente);
						while(verifica_conta_disponivel==0)
						{
							printf("Conta indispon�vel");
							getch();
							system("cls");
							verifica_saque=3;
							while(verifica_saque==3)
							{
								printf("Deseja depositar em outra conta: [1]SIM [2]N�O\n");
								scanf("%i",&verifica_saque);
								if(verifica_saque==1)
								{
									printf("Digite o c�digo de conta corrente: ");
									scanf("%i",&cod_conta);
									verifica_conta_disponivel=busca_conta_disponivel(cod_conta,lista_cliente);
								}
								else if(verifica_saque==2)
								{
									verifica_saque=0;
									system("cls");
									goto inicio;
								}
								else
								{
									verifica_deposito=3;
									printf("Op��o Inv�lida!");
									getch();
									system("cls");
								}
							}
						}
						printf("Digite o valor a ser sacado: ");
						scanf("%f",&nova_transacao.valor);
						saldo=verifica_saldo(cod_conta,lista_conta_corr);

						while(nova_transacao.valor<=0 || saldo<nova_transacao.valor)
						{
							printf("\nValor inv�lido!");
							getch();
							system("cls");
							printf("Digite o valor a ser sacado: ");
							scanf("%f",&nova_transacao.valor);
							saldo=verifica_saldo(cod_conta,lista_conta_corr);
						}

						nova_transacao.cod_transacao=l_transacao->nelem+1;
	    				nova_transacao.origem=cod_conta;
	    				nova_transacao.destino=NULL;
	    				nova_transacao.tipo_transacao='S';
						inserir_transacao(&l_transacao,nova_transacao);
						alter_conta_corr=(lista_conta_corr,lista_conta_corr.quant_conta_corr,cod_conta);
						lista_conta_corr.l_conta_corr[alter_conta_corr].saldo=lista_conta_corr.l_conta_corr[alter_conta_corr].saldo-nova_transacao.valor;

						printf("Sacado com sucesso!");
						getch();
						system("cls");
					break;

						case 3:
							printf("Digite o c�digo de conta corrente a ser retirado o valor: ");
							scanf("%i",&cod_conta);

							verifica_conta_disponivel=busca_conta_disponivel(cod_conta,lista_cliente);

							printf("Digite o c�digo de conta corrente a receber o valor: ");
							scanf("%i",&transferencia);

							verifica_conta=busca_conta_disponivel(transferencia,lista_cliente);

							while(verifica_conta_disponivel==0 || verifica_conta==0)
							{
								printf("Conta indispon�vel");
								getch();
								system("cls");
								verifica_transferencia=3;
								while(verifica_transferencia==3)
								{
									printf("Deseja realizar outra transfer�ncia: [1]SIM [2]N�O\n");
									scanf("%i",&verifica_transferencia);
									if(verifica_transferencia==1)
									{
										printf("Digite o c�digo de conta corrente a ser retirado o valor: ");
										scanf("%i",&cod_conta);

										verifica_conta_disponivel=busca_conta_disponivel(cod_conta,lista_cliente);

										printf("Digite o c�digo de conta corrente a receber o valor: ");
										scanf("%i",&transferencia);

										verifica_conta=busca_conta_disponivel(transferencia,lista_cliente);
									}
									else if(verifica_transferencia==2)
									{
										verifica_transferencia=0;
										system("cls");
										goto inicio;
									}
									else
									{
										verifica_transferencia=3;
										printf("Op��o Inv�lida!");
										getch();
										system("cls");
									}
								}
							}
							printf("Digite o valor a ser transferido: ");
							scanf("%f",&nova_transacao.valor);
							saldo=verifica_saldo(cod_conta,lista_conta_corr);

							while(nova_transacao.valor<=0 ||saldo<nova_transacao.valor)
							{
								printf("\nValor inv�lido!");
								getch();
								system("cls");
								printf("Digite o valor a ser transferido: ");
								scanf("%f",&nova_transacao.valor);
								saldo=verifica_saldo(cod_conta,lista_conta_corr);

							}
							nova_transacao.cod_transacao=l_transacao->nelem+1;
							nova_transacao.origem=cod_conta;
   							nova_transacao.destino=transferencia;
   							nova_transacao.tipo_transacao='T';
							inserir_transacao(&l_transacao,nova_transacao);
							alter_conta_corr=(lista_conta_corr,lista_conta_corr.quant_conta_corr,cod_conta);
							alter_conta_transf=(lista_conta_corr,lista_conta_corr.quant_conta_corr,transferencia);
							lista_conta_corr.l_conta_corr[alter_conta_corr].saldo=lista_conta_corr.l_conta_corr[alter_conta_corr].saldo-nova_transacao.valor;
							lista_conta_corr.l_conta_corr[alter_conta_transf].saldo=lista_conta_corr.l_conta_corr[alter_conta_corr].saldo+nova_transacao.valor;

							printf("Transferido com sucesso!");
							getch();
							system("cls");
						break;

							case 4:
								//OBS: LISTA TODAS
								printf("\nLista de Transa��es\n\n");
								imprimir_transacao(&l_transacao);
								getch();
								system("cls");

							break;

								case 5:
									//VOLTAR
									goto inicio;
								break;

				}
			break;

			case 3:
				encerra=1;
			break;

			default:
				printf("Op��o inv�lida");
				getch();
				system("cls");
		}

	}

}

int main()
{
	//ADMITE ACENTO
	setlocale(LC_ALL,"Portuguese");

	lista_cliente lista_cliente;
	lista_conta_corr lista_conta_corr;
	l_transacao l_transacao;

	inicializa_lista(&l_transacao,&lista_conta_corr,&lista_cliente);
	inserir_conta_correntes(&lista_conta_corr);
	interface(lista_cliente,lista_conta_corr,&l_transacao);
}
