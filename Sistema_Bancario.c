#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//maximo de cliente cadastrados
#define MAX 50

typedef struct cliente cliente;
typedef struct conta_corrente conta_corrente;
typedef struct transacao transacao;
typedef struct lista_cliente lista_cliente;
typedef struct lista_conta_corr lista_conta_corr;
typedef struct no_transacao no_transacao;
typedef struct l_transacao l_transacao;



//estruturas transacao, conta corrente e cliente
struct transacao
{
	int cod_transacao;
	char tipo_transacao;
	int origem;
	int destino;
	float valor;
};

struct conta_corrente
{
	int cod_conta;
	float saldo;
	transacao lista_transacao;
};

struct cliente
{
	int cod_cli;
	char nome_cli[50];
	char telefone[20];
	int conta_cli;
};

//TAD lista sequencial cliente, conta corrente
struct lista_cliente{
    int quant_cli;// numero de elementos atual da lista
    cliente l_cli[MAX];
};

struct lista_conta_corr{
    int quant_conta_corr;// numero de elementos atual da lista
    conta_corrente l_conta_corr[MAX];
};

//TAD lista encadeada transacao
struct no_transacao
{
	transacao info;
	no_transacao *proximo;
};
struct l_transacao
{
	int nelem;
	no_transacao *head;
};

//inicializa listas
void inicializa_lista(l_transacao *lista_transacao,lista_conta_corr *lista_conta_corr,lista_cliente *lista_cliente)
{
    lista_cliente->quant_cli=0; //inicializa cliente
    lista_conta_corr->quant_conta_corr=0;//inicializa conta corrente
    
    lista_transacao = (l_transacao*) malloc(sizeof(l_transacao));//aloca espa�o para a transacao
    lista_transacao->head = NULL;// inicializa a lista.
    lista_transacao->nelem = 0; // numero de elementos come�a em zero.    
}

//insere contas correntes
void inserir_conta_correntes(lista_conta_corr *lista_conta_corr)
{
	int i;
	for(i=0;i<MAX;i++)
	{
		lista_conta_corr->l_conta_corr[i].cod_conta=i+1;	
		lista_conta_corr->l_conta_corr[i].saldo=0;
		lista_conta_corr->quant_conta_corr++;
	}
}

//insere previamente um cliente
void inserir_cliente2(lista_cliente *lista_cliente)
{
	lista_cliente->l_cli[0].cod_cli=1;
	lista_cliente->l_cli[0].conta_cli=2;
	lista_cliente->quant_cli++;
}

//lista todas as transacoes
void imprimir_transacao(l_transacao *lista)
{
    no_transacao *posicao;
    posicao = lista->head;
    int i;
	while(posicao != NULL)
	{	
		printf("Codigo: %i\n", posicao->info.cod_transacao);
		printf("Destino: %i\n", posicao->info.destino);
		printf("Origem: %i\n", posicao->info.origem);
		printf("Tipo: %c\n", posicao->info.tipo_transacao);		
		printf("Valor: %f\n", posicao->info.valor);
		printf("\n\n");
		posicao = posicao->proximo;
		i++;
	}
	
}
//busca cliente para inserir novo cliente na posicao correta
int busca_cliente(lista_cliente lista,cliente novo_cliente)
{
	int i=0;
	//percorre lista para encontrar posicao de insercao do cliente
	while(lista.l_cli[i].cod_cli<novo_cliente.cod_cli)
	{
		i++;
	}
	return i;
}

//verifica contas correntes disponiveis
int busca_conta_disponivel(int conta,lista_cliente lista_cliente)
{
	int j;
		for(j=0;j<lista_cliente.quant_cli;j++)
		{
			if(conta==lista_cliente.l_cli[j].conta_cli)
			{
				return 1;
			}
		}
		
		return 0;
}
//verifica contas correntes existentes
int busca_contas_existentes(int conta,lista_conta_corr lista_conta_corr)
{
	int j;
		for(j=0;j<lista_conta_corr.quant_conta_corr;j++)
		{
			if(conta==lista_conta_corr.l_conta_corr[j].cod_conta)
			{
				return 0;
			}
		}
		
		return 1;
}

//busca posi��o da conta corrente desejada para inser��o de saldo inicial
int busca_posicao_conta(int conta,lista_conta_corr lista_conta_corr)
{
	int j;
		for(j=0;j<lista_conta_corr.quant_conta_corr;j++)
		{
			if(conta==lista_conta_corr.l_conta_corr[j].cod_conta)
			{
				return j;//retorna posi��o da conta corrente buscada
			}
		}
		
		return 0;
}

//inserir transacoes
void inserir_transacao(l_transacao *l_transacao,transacao nova_transacao)
{
	no_transacao *elemento;
	no_transacao *aux;
	
	if(l_transacao->nelem==0)
	{
		elemento=malloc(sizeof(no_transacao));
		elemento->info=nova_transacao;
		elemento->proximo=NULL;
		l_transacao->head=elemento																				;
		l_transacao->nelem=1;
	}
	else
	{
		elemento=(no_transacao*)malloc(sizeof(no_transacao));
		elemento->info=nova_transacao;
		aux=l_transacao->head;
		l_transacao->head=elemento;
		elemento->proximo=aux;
		l_transacao->nelem++;
	}
}

//insere cliente
void inserir_cliente(cliente novo_cliente,conta_corrente nova_conta,lista_cliente *lista_cliente, lista_conta_corr *lista_conta_corr)
{
	int pos,i,pos_conta;
	
	//verifica se a lista de clientes esta cheia
	if(lista_cliente->quant_cli==MAX)
	{
		printf("Lista de cliente est� cheia");
	}
	else
	{
		//verifica se a lista esta vazia
		if(lista_cliente->quant_cli==0)
		{
			//insere dados na posicao 0
			lista_cliente->l_cli[0].cod_cli=lista_cliente->quant_cli+1;
			strcpy(lista_cliente->l_cli[0].nome_cli,novo_cliente.nome_cli);
			strcpy (lista_cliente -> l_cli [0].telefone,novo_cliente.telefone);
			
			
			
			//recebe numero de conta corrente
			lista_cliente->l_cli[0].conta_cli=novo_cliente.conta_cli;
			
			//insere saldo inicial
			pos_conta=busca_posicao_conta(novo_cliente.conta_cli,*lista_conta_corr);
			lista_conta_corr->l_conta_corr[pos_conta].saldo=nova_conta.saldo;
			
			
			//aumenta quantidade de clientes
			lista_cliente->quant_cli++;			
			
			
		}
		else
		{
			//busca posicao de insercao
			pos=busca_cliente(*lista_cliente,novo_cliente);
			
			//movimenta clientes para frente para armazenar novo cliente na lista
			for(i=lista_cliente->quant_cli;i>pos;i--)
			{
				lista_cliente->l_cli[i]=lista_cliente->l_cli[i-1];			
			}
			
			//inserir cliente no local apropriado
			lista_cliente->l_cli[i].cod_cli=lista_cliente->quant_cli+1;
			strcpy(lista_cliente->l_cli[pos].nome_cli,novo_cliente.nome_cli);
			strcpy (lista_cliente -> l_cli [pos].telefone,novo_cliente.telefone);

			//recebe numero de conta corrente
			lista_cliente->l_cli[pos].conta_cli=novo_cliente.conta_cli;
			
			//insere saldo inicial
			pos_conta=busca_posicao_conta(novo_cliente.conta_cli,*lista_conta_corr);
			lista_conta_corr->l_conta_corr[pos_conta].saldo=nova_conta.saldo;	
			
						
			//aumenta quantidade de clientes
			lista_cliente->quant_cli++;			
			
		}
	}
}

//busca transa��es da conta corrente desejada
void consulta_transacao(int cod_conta,l_transacao *l_transacao)
{	
	no_transacao *posicao;
    posicao = l_transacao->head;
    
    while(posicao != NULL)  
    {
        if((posicao->info.destino == cod_conta)||(posicao->info.origem == cod_conta))
        {
        	
			printf("Destino: %i Origem: %i Tipo:%c Valor:%f",posicao->info.destino,posicao->info.origem,posicao->info.tipo_transacao,posicao->info.valor);
			
			posicao = posicao->proximo;
		}
		else
            posicao = posicao->proximo;
        
    }
}


// consulta se um cliente existe por meio de busca sequencial pela codigo de conta corrente
int consultar_cliente (lista_cliente lista_cliente,int n,int cod_conta)
{
 int i;
 for (i=0;i<n;i++)
 {
     if (lista_cliente.l_cli[i].conta_cli==cod_conta)
     {
       printf("Nome: %s || Telefone: %s",lista_cliente.l_cli[i].nome_cli, lista_cliente.l_cli[i].telefone);
       return i;
     }
     return -1;
  }
}


void interface(lista_cliente lista_cliente,lista_conta_corr lista_conta_corr,l_transacao *l_transacao)
{	
    
	cliente novo_cliente;
	conta_corrente nova_conta;
	transacao nova_transacao;
	int codigo;
	int cod_conta, cod_conta_alter;
	int i, verifica_conta,verifica_conta_disponivel,verifica_conta_existente,nelem,cont,pos_alter,verifica_cliente;
	
//	printf("%i",l_transacao->nelem);
	printf ("Digite o numero de clientes que deseja inserir: ");
	scanf ("%i",&nelem);
	
	for (cont=0;cont<nelem;cont++)
	{
		fflush(stdin);
		printf("Entre com o nome do cliente: \n");
    	fflush(stdin);
		scanf("%[^\n]s",&novo_cliente.nome_cli);
		fflush(stdin);
		printf ("Entre com o telefone do cliente: \n");
    	fflush(stdin);
		scanf("%[^\n]s",&novo_cliente.telefone);
		fflush(stdin);
	
		//lista contas disponiveis
		printf("\n\nContas Correntes Disponiveis\n\n");
    	for(i=0;i<lista_conta_corr.quant_conta_corr;i++)
    	{
	    	verifica_conta=busca_conta_disponivel(lista_conta_corr.l_conta_corr[i].cod_conta,lista_cliente);
	    	if(verifica_conta==0)
    		{
	    		printf("%i\n",lista_conta_corr.l_conta_corr[i].cod_conta);
			}          
	    }
		printf("Digite o numero da conta corrente: \n");
	    fflush(stdin);
		scanf("%d",&novo_cliente.conta_cli);
	    fflush(stdin);
	    
		//verifica se a conta corrente digitada � valida
		verifica_conta_disponivel=busca_conta_disponivel(novo_cliente.conta_cli,lista_cliente);
    	verifica_conta_existente=busca_contas_existentes(novo_cliente.conta_cli,lista_conta_corr);
		
		while(verifica_conta_disponivel==1 || verifica_conta_existente==1)
	    {
	    	printf("\nConta Indisponivel");
	    	printf("\nDigite o numero da conta corrente: \n");
			fflush(stdin);
			scanf("%i",&novo_cliente.conta_cli);
    		fflush(stdin);
			verifica_conta_disponivel=busca_conta_disponivel(novo_cliente.conta_cli,lista_cliente);
			verifica_conta_existente=busca_contas_existentes(novo_cliente.conta_cli,lista_conta_corr);
		}
    	
		printf("Digite o deposito inicial: \n");
		fflush(stdin);
		scanf("%f",&nova_conta.saldo);
		fflush(stdin);
		//verifica se o deposito inicial esta acima do valor permitido
		while(nova_conta.saldo<50)
		{
			printf("\nSaldo Invalido! Deposite um valor acima de 50 reais\n");		
			fflush(stdin);
			scanf("%f",&nova_conta.saldo);	
			fflush(stdin);
		}	
		inserir_cliente(novo_cliente,nova_conta,&lista_cliente,&lista_conta_corr);
		
	
	    nova_transacao.cod_transacao=l_transacao->nelem+1;
	    nova_transacao.origem=NULL;
	    nova_transacao.destino=novo_cliente.conta_cli;
	    nova_transacao.tipo_transacao='D';
	    nova_transacao.valor=nova_conta.saldo;
		inserir_transacao(&l_transacao,nova_transacao);
	}
	
    printf("\nLista de Contas\n\n");
    for(i=0;i<lista_conta_corr.quant_conta_corr;i++)
    {
    	printf("Conta: %i	|Saldo:%f\n",lista_conta_corr.l_conta_corr[i].cod_conta,lista_conta_corr.l_conta_corr[i].saldo);
	}
	
	printf("\nLista de Transacoes\n\n");
	imprimir_transacao(&l_transacao);
	
	
	//BUSCAR 
	printf("Digite o numero da conta corrente: ");
	scanf("%i", &cod_conta);
	fflush(stdin);
	verifica_cliente = consultar_cliente (lista_cliente,lista_cliente.quant_cli,cod_conta);
	consulta_transacao(cod_conta,&l_transacao);
	
	//alterar cliente
	printf("\n");
	printf("Digite o numero da conta corrente do cliente o qual deseja alterar os dados cadastrais: ");
	scanf("%i", &cod_conta_alter);

	pos_alter = consultar_cliente (lista_cliente, lista_cliente.quant_cli,cod_conta_alter);
	alterar_cliente (&lista_cliente, pos_alter);
	
	
	printf("\nLista de Cliente\n\n");
    for(i=0;i<lista_cliente.quant_cli;i++)
    {
                    
    	printf("Codigo: %i    |Nome: %s  |Telefone:%s	|Conta:%i\n",lista_cliente.l_cli[i].cod_cli,lista_cliente.l_cli[i].nome_cli,lista_cliente.l_cli[i].telefone,lista_cliente.l_cli[i].conta_cli);
                    
    }
	
	
	//deposito
	printf("Digite o codigo de conta corrente: ");
	scanf("%i",&cod_conta);
	
	verifica_conta_disponivel=busca_conta_disponivel(cod_conta,lista_cliente);
	if(verifica_conta_disponivel==0)
	{
		printf("Conta indisponivel");	
		//volta para menu incial
	}
	else
	{
		printf("Digite o valor a ser depositado: ");
		scanf("%f",&nova_transacao.valor);
		if(nova_transacao.valor>0)
		{
			
			nova_transacao.cod_transacao=l_transacao->nelem+1;
	    	nova_transacao.origem=NULL;
	    	nova_transacao.destino=cod_conta;
	    	nova_transacao.tipo_transacao='D';
			inserir_transacao(&l_transacao,nova_transacao);
		}
	}
	
}

int main()
{
	lista_cliente lista_cliente;
	lista_conta_corr lista_conta_corr;
	l_transacao l_transacao;
	
	inicializa_lista(&l_transacao,&lista_conta_corr,&lista_cliente);
	inserir_conta_correntes(&lista_conta_corr);
//	inserir_cliente2(&lista_cliente);
	interface(lista_cliente,lista_conta_corr,&l_transacao);
}	

